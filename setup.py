import sqlite3 as sqlite
import sys
import urllib, urllib2
import json
import time
import os
import subprocess

# check if we are root
if(os.geteuid() != 0):
	print "Please run this using `sudo python setup.py`"
	sys.exit(3)

DB = './agrinet.db'
con = None
locationHash = None
locationName = None
locationLocation = None
locationClient = None
setupComplete = False

# cloud urls
URL_SETUP = "http://agrinet-cloud.herokuapp.com/devices/setup"
URL_ADDON = "http://agrinet-cloud.herokuapp.com/devices/addon"

def installBootScript():
	print "Registering boot script..."

	# copy the startup script, make it executable and register it
	os.system('cp /home/pi/agrinet-rpi-server/agrinet /etc/init.d/agrinet')
	os.system('chmod 755 /etc/init.d/agrinet')
	os.system('/usr/sbin/update-rc.d agrinet defaults')
	
	# reboot
	raw_input("Press enter to reboot...")
	os.system('shutdown -r now')

# boolean: check if the net is connected or not
def internetConnected():
	req = urllib2.Request(URL_SETUP)
	
	try:
		res = urllib2.urlopen(req)
	except urllib2.URLError, e:
		return False
	else:
		return True

# attempt to connect to sqlite
try:
	con = sqlite.connect(DB)
	
	with con:		
		# return a dictionary 
		con.row_factory = sqlite.Row
		
		# cursor to the connection
		cur = con.cursor()
		
		# check if the device has been authenticated yet
		cur.execute('SELECT value FROM settings WHERE name = ?', ("locationHash",))
		locationHash = cur.fetchone()[0]
		
		if locationHash != None:
			# setup has already been performed
			print "Device has already been configured."
			sys.exit(1)
		else:
			print "Checking internet connection..."
			# check if the internet is connected
			if not internetConnected():
				print "Please connect to the internet before running this program."
				sys.exit(2)
			else:
				print 'Updating firmware...'
				subprocess.call(['/usr/bin/rpi-update'])
				print 'Please complete setup before reboot'
				first = raw_input("Is this the first pi on site? (yes/NO) = ")
				
				# check what the user said
				if first == "yes" or first == "y":
					# first pi
					
					# loop until we get a valid name
					while locationName is None or locationName == "":
						locationName = raw_input("Site Name = ")
						
						# we have an invalid name
						if locationName is None or locationName == "":
							print "Please enter a valid name"
						else:
							pass
					
					# loop until we get a valid name
					while locationLocation is None or locationLocation == "":
						locationLocation = raw_input("Site Location = ")
						
						# we have an invalid name
						if locationLocation is None or locationLocation == "":
							print "Please enter a valid location"
						else:
							pass
					
					# loop until we get a valid name
					while locationClient is None or locationClient == "" or not locationClient.isdigit():
						locationClient = raw_input("Client ID = ")
						
						# we have an invalid name
						if locationClient is None or locationClient == "" or not locationClient.isdigit():
							print "Please enter a valid client id"
						else:
							pass
					
					print "Connecting to server..."
					
					# we have valid details
					locationData = {}
					locationData["name"] = locationName
					locationData["location"] = locationLocation
					locationData["cid"] = locationClient
					
					# encode the data for sending to the server
					postData = [('json', json.dumps(locationData))]
					postData = urllib.urlencode(postData)
					
					# send a post request
					req = urllib2.Request(URL_SETUP, postData)
					req.add_header("Content-type", "application/x-www-form-urlencoded")
					
					# connect to the server
					try:
						res = urllib2.urlopen(req)
					except urllib2.URLError, e:
						# sometimes there is no error code
						try:
							if e.code == 404:
								print "Could not find the authenticator. Something is most likely wrong with the server."
							elif e.code == 406:
								print "Invalid data. Please check all data and try again."
							else:
								print "Could not connect to the authenticator. Error code: "+str(e.code)
						except Exception, e:
							print "A connection error occured: "+str(e)
					else:
						# we got a 200 response
						locationHash = res.read()
						
						# key, value pairs for db updating 
						values = {}
						values['locationHash'] = locationHash
						values['locationName'] = locationName
						values['locationLocation'] = locationLocation
						values['setupTime'] = int(time.time())
						
						# db update
						for key in values:
							cur.execute('UPDATE settings SET value = ? WHERE name = ?', (values[key], key))
							
							# commit the changes
							con.commit()
						
						setupComplete = True
						print "Setup complete."
						print "Location ID: "+locationHash
				else:
					# this is not the first pi
					
					# loop until we get a valid hash
					while locationHash is None or locationHash == "":
						locationHash = raw_input("Site ID = ")
						
						# we have an invalid name
						if locationHash is None or locationHash == "":
							print "Please enter a valid id"
						else:
							pass
					
					# lets get the details for this location from the server
					locationData = {}
					locationData['hash'] = locationHash
					
					# encode the data for sending to the server
					postData = [('json', json.dumps(locationData))]
					postData = urllib.urlencode(postData)
					
					# send a post request
					req = urllib2.Request(URL_ADDON, postData)
					req.add_header("Content-type", "application/x-www-form-urlencoded")
					
					print "Connecting to server..."
					
					# connect to the server
					try:
						res = urllib2.urlopen(req)
					except urllib2.URLError, e:
						# sometimes there is no error code
						try:
							if e.code == 404:
								print "Could not find the authenticator. Something is most likely wrong with the server."
							elif e.code == 400:
								print "Invalid location id"
							else:
								print "Could not connect to the authenticator. Error code: "+str(e.code)
						except Exception, e:
							print "A connection error occured: "+str(e)
					else:
						# we got a 200 response
						locationInfoData = json.loads(res.read())
						
						# key, value pairs for db updating 
						values = {}
						values['locationHash'] = locationInfoData['hash']
						values['locationName'] = locationInfoData['name']
						values['locationLocation'] = locationInfoData['location']
						values['setupTime'] = int(time.time())
						
						# db update
						for key in values:
							cur.execute('UPDATE settings SET value = ? WHERE name = ?', (values[key], key))
							
							# commit the changes
							con.commit()
						
						print "Setup complete."
						print "Location ID: "+locationHash
						
except sqlite.Error, e:
	print "SQLite connect error: %s: " % e.args[0]
finally:
	# close the database connection if it is open
	if con:
		con.close()
		
	if setupComplete is True:
		installBootScript()
