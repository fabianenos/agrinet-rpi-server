import RPi.GPIO as GPIO
from time import sleep, time
import sqlite3 as sqlite
import sys
import urllib, urllib2
import json
import os
from random import randint
import logging

DB = '/home/pi/agrinet-rpi-server/agrinet.db'
LOGFILE = '/home/pi/agrinet-rpi-server/logs.log'
con = None
locationHash = None
PIDFILE = '/home/pi/agrinet-rpi-server/pid'
POLL_TIME = 300 # number of seconds between each polling

# cloud urls
URL_PUSH = "http://agrinet-cloud.herokuapp.com/devices/push"

# read values of the sensors and return json
def readSensors():
	rData = []
	rData.append({"type": 1, "value": randint(40,80)}) # water
	rData.append({"type": 1, "value": randint(40,80)}) # water
	rData.append({"type": 1, "value": randint(40,80)}) # water
	
	rData.append({"type": 2, "value": randint(40,50)}) # light
	rData.append({"type": 3, "value": randint(30,80)}) # humidity
	rData.append({"type": 3, "value": randint(30,80)}) # humidity
	rData.append({"type": 4, "value": randint(30,45)}) # temperature
	rData.append({"type": 4, "value": randint(30,45)}) # temperature
	
	return rData

# logging init
logging.basicConfig(filename=LOGFILE,level=logging.DEBUG,format='%(asctime)s %(message)s')

# attempt to connect to sqlite
try:
	con = sqlite.connect(DB)
	
	# set the correct mode
	GPIO.setmode(GPIO.BCM)
	
	with con:		
		# return a dictionary 
		con.row_factory = sqlite.Row
		
		# cursor to the connection
		cur = con.cursor()
		
		# check if the device has been authenticated yet
		cur.execute('SELECT value FROM settings WHERE name = ?', ("locationHash",))
		locationHash = cur.fetchone()[0]
		
		if locationHash == None:
			# setup was not performed or an error occured
			logging.error("Device not configured. Please run the configuration program")
			sys.exit(1)
			
		# lets create a pid file
		pid = str(os.getpid())
		
		file(PIDFILE, 'w').write(pid)
		
		# main loop
		while True:
			# read the values from the sensors
			readings = readSensors()
			
			# fetch the read count
			cur.execute('SELECT value FROM settings WHERE name = ?', ("readCount",))
			readCount = cur.fetchone()[0]
			
			# default value
			if readCount == None:
				readCount = 0
			else:
				readCount = int(readCount)
			
			# read each tuple in the array
			for key in readings:
				cur.execute('INSERT INTO readings(type,value,read) VALUES(?,?,?)', (key['type'],key['value'], readCount))
				
				# commit the changes
				con.commit()
				
			readCount = readCount+1
			# update the read count
			cur.execute('UPDATE settings SET value = ? WHERE name = ?', (str(readCount), "readCount"))
			con.commit()
			
			# read all the values that have not been pushed from the database
			cur.execute("SELECT read,type,value FROM readings WHERE pushed = 'false'")
			readings = cur.fetchall()
			
			pushData = []
			# iterate over the values
			for reading in readings:
				pushData.append({"type": reading['type'], "value": reading['value'], "read": reading['read']})
			
			# encode the data for sending to the server
			postData = [('json', json.dumps(pushData)), ('location', locationHash)]
			postData = urllib.urlencode(postData)
			
			# send a post request
			req = urllib2.Request(URL_PUSH, postData)
			req.add_header("Content-type", "application/x-www-form-urlencoded")
			
			# connect to the server
			try:
				res = urllib2.urlopen(req)
			except urllib2.URLError, e:
				# sometimes there is no error code
				try:
					if e.code == 404:
						logging.error("Could not find the pusher. Something is most likely wrong with the server.")
					elif e.code == 400:
						logging.error("The device location is no longer valid. Please reconfigure the device.")
					elif e.code == 500:
						logging.error("Invalid data being sent.")
					elif e.code == 406:
						logging.info("One or more values could not be pushed.")
					else:
						logging.error("Could not connect to the pusher. Error code: "+str(e.code))
				except Exception, e:
					logging.error("A connection error occured: "+str(e))
			else:
				# we got a 200 response
				result = res.read()
				
				# update the database
				cur.execute("UPDATE readings SET pushed = 'true'")
				con.commit()
				
				logging.info("Push result = "+result)
			
			sleep(POLL_TIME)
except sqlite.Error, e:
	logging.error("SQLite connect error: %s: " % e.args[0])
finally:
	# close the database connection if it is open
	if con:
		con.close()
	
	# remove the pid file
	os.unlink(PIDFILE)
